#!/bin/bash

# Scan des données brutes
# Utilisation : 
# scan_raw -n FR -d 1

PGHOST=resif-pgprod.u-ga.fr
SEEDTREE_BIN_DIR=/home/sysop/seedtree5/src/

autofs_bud_path() {
    # Pour un nom de fichier, renvoie le chemin absolu dans l'automontage bud
    # Si le nom de fichier n'est pas bon, ne renvoie rient
    # regex pour vérifier que le nom est bien construit et capurer les infos du nom de fichier
    # Tester : https://regex101.com/r/PSvVkX/1/
    re_filename="^([0-9A-Z]{1,2})\.([0-9A-Z]+)\.([0-9]{2}|)\.([A-Z1-3]{3})\.D\.([0-9]{4})\.([0-9]{3})$"
    while IFS= read -r filename; do
        if [[ "${filename}" =~ $re_filename ]] ; then
            echo "/mnt/auto/archive/rawdata/bud/${BASH_REMATCH[5]}/${BASH_REMATCH[1]}/${BASH_REMATCH[2]}/${BASH_REMATCH[4]}.D/${filename}"
        fi
    done
}


usage() {
    echo "Usage: $0 [-n NETWORK[,NETWORK,...]] [-d DAYS] [-t]" 1>&2
    echo "Scan latest SUMMER bud snapshot and register new files in seedtree database."
    echo "Options :"
    echo "  -n NETWORK[,NETWORK]    if specified, then only NETWORK (as extended FDSN network code) will be scanned"
    echo "                          a list of coma separated networks can be passed."
    echo "  -d DAYS                 if specified, scan BUD for all files created DAYS days ago"
    echo "  -t                      test mode. Will scan and pretend to index"
    echo "  -p                      seedtree schema prefix (default _)"
}

echolog() {
    echo "$(date '+%Y-%m-%d %H:%M:%S') $@"
}

dryrun=0                    # On va indexer
filetree_mode="--sync"          # Mode incrémental ou reindex total ?
declare -a networks         # Notre tableau de réseaux à scanner
networks_base_request="select network_id||':'||extnet from (select a.network_id, case when network ~ '^[0-9XYZ]' then network||start_year else network end extnet from networks a inner join (select distinct network_id from rbud) b on a.network_id=b.network_id) c"
schema_prefix="_"
# Récupérer la liste des réseaux
while getopts "n:d:p:t" options; do
    case "${options}" in
        n)
            IFS=',' read -ra nets <<< "${OPTARG}"
            for n in ${nets[*]}; do
                where_clause+="extnet='$n' OR "
            done
            mapfile -t networks < <(psql -qtA -h $PGHOST -d resifInv-Prod -U resifinv_ro -c "$networks_base_request WHERE ${where_clause% OR }")
            echolog "${#networks[*]} network(s) to scan."
        ;;
        d)
            re_isanum='^[0-9]+$'
            if [[ ${OPTARG} =~ $re_isanum ]]; then
                sql_opts+=" AND created_at > date_trunc('day', now()) - interval '${OPTARG} days'"
                filetree_mode="--insert --forceupdate"
            else
                echolog "Error: Option -d must be an integer"
                usage
            fi
            ;;
        t)
            dryrun=1
            ;;
        p)
            schema_prefix=${OPTARG}
        ;;
        *)
            usage
            exit 0
            ;;
    esac
done

if [[ ${#networks[*]} -eq 0 ]]; then
    # Si on n'a pas précisé de network en argument, alors on les prend tous dans notre liste
    mapfile -t networks < <(psql -qtA -h $PGHOST -d resifInv-Prod -U resifinv_ro -c "$networks_base_request")
fi

for idnet in ${networks[*]}; do
    # idnet est la concaténation du networkid et du nom de réseau étendu, par ex 39:FR
    netid=${idnet%:*}
    net=${idnet#*:}
    echolog "==== Scanning $net ===="
    raw_schema="$schema_prefix"$(echo $net | tr '[:upper:]' '[:lower:]')"raw"
    seedtree-admin --create $raw_schema "Données brutes de $net" > /dev/null 2>&1
    # D'abord on cherche les fichiers qui ont été supprimés (présents dans filetree mais pas dans rbud)
    rbudlist=$(psql -qtA -h $PGHOST -U resifinv_ro -d resifInv-Prod -c "select source_file from rbud where network_id='$netid' order by 1")
    filetreelist=$(psql -qtA -h $PGHOST -U seedtree5 -d seedtree5 -c "select basename from $raw_schema.files order by 1")
    # La commande comm, avec des listes triées permet de récupérer tout ce qui est dans filetree mais pas dans rbud. O_o
    to_delete=$(comm -23 <(echo "$filetree") <(echo "$rbudlist"))
    # Cherche tous les fichiers et transforme les chemins en chemin autofs
    filelist=$(psql -qtA -h $PGHOST -U resifinv_ro -d resifInv-Prod -c "select source_file from rbud where network_id='$netid' $sql_opts")
    nbfiles=$(wc -w <<< $filelist)
    nb_to_delete=$(wc -w <<< $to_delete)
    # Maintenant on indexe
    echolog "== Indexing $net to schema $raw_schema =="
    echolog "   Removing $nb_to_delete files that disapeared (filetree.py --delete)"
    [ $dryrun -eq 0 ] && \
      echo "$to_delete" | autofs_bud_path | $SEEDTREE_BIN_DIR/filetree.py --delete $raw_schema
    if [[ $nbfiles -ne 0 ]]; then
        echolog "   Adding $nbfiles new files to filetree with $filetree_mode mode"
        [ $dryrun -eq 0 ] && \
          echo "$filelist" | autofs_bud_path | $SEEDTREE_BIN_DIR/filetree.py $filetree_mode  $raw_schema
    fi
    echolog "   Starting seedtree --sync $raw_schema"
    [ $dryrun -eq 0 ] && \
      $SEEDTREE_BIN_DIR/seedtree5.py --sync $raw_schema

    # Replace old schema and recompute availability
    prod_schema="_"$(echo $net | tr '[:upper:]' '[:lower:]')"raw"
    psql -h $PGHOST -U seedtree5 -d seedtree5 -c "BEGIN WORK; ALTER SCHEMA $prod_schema RENAME TO back${prod_schema}; ALTER SCHEMA $raw_schema RENAME TO ${prod_schema}; COMMIT;"
done
psql -h resif-pgprod.u-ga.fr -U wsavailability_refresh -d seedtree5 -c "select update_wsavailability_traces();"

echolog "Indexation finished"
