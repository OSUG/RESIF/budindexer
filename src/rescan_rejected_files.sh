#!/usr/bin/env bash

# Ce script prend pour un schéma donné la liste des fichiers rejetés et relance une passe de scan
# À la fin, il envoie à Zabbix le nouveau nombre de fichiers
#

PGHOST=resif-pgprod.u-ga.fr
SEEDTREE_BIN_DIR=/home/sysop/seedtree5/src/

if (( $# != 1 )); then
   >&2 echo "Pass a schema name as argument"
fi

raw_schema=$1

rejected_files=$(psql -qtA -h $PGHOST -U seedtree5 -d seedtree5 -c "select count(*) from $raw_schema.rejectedminiseedfiles")
echo "Indexing $rejected_files rejected files"
count=0
for i in $(psql -qtA -h $PGHOST -U seedtree5 -d seedtree5 -c "select dirname||basename from ${raw_schema}.files a right join (select fileindex from ${raw_schema}.rejectedminiseedfiles) b ON a.fileindex=b.fileindex"); do
    count+=1
    echo $i | $SEEDTREE_BIN_DIR/filetree.py --insert --forceupdate $raw_schema
    if (( $count == 50 )); then
        echo "Scanning $count files"
        $SEEDTREE_BIN_DIR/seedtree5.py --sync $raw_schema
        rejected_files=$(psql -qtA -h $PGHOST -U seedtree5 -d seedtree5 -c "select count(*) from $raw_schema.rejectedminiseedfiles")
        echo "Rejected files: $rejected_files"
        count=0
    fi

done

$SEEDTREE_BIN_DIR/seedtree5.py --sync $raw_schema

rejected_files=$(psql -qtA -h $PGHOST -U seedtree5 -d seedtree5 -c "select count(*) from $raw_schema.rejectedminiseedfiles")
echo "There are still $rejected_files rejected files. Reporting to Zabbix"
zabbix_sender -c /etc/zabbix/zabbix_agentd.conf -k "seedtree.rejectedfiles[${raw_schema}]" -o $rejected_files
