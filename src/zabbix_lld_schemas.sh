#!/usr/bin/env bash
# Returns a data structure suitable for Zabbix LLD rule.
# Discovering all schemas

# {"data":[
#     "{#SCHEMA}":"_graw",
#     ]
# }
#
json_lld="{\"data\":["
for schema in $(psql -qtA -h resif-pgprod.u-ga.fr -U seedtree5 -d seedtree5 -c "select schema_name from information_schema.schemata where schema_name ~ '^_'"); do
     if [[ "$schema" == *raw ]]; then
       json_lld+="{\"{#SCHEMA}\": \"$schema\", \"{#DATATYPE}\": \"raw\"},"
     else
       json_lld+="{\"{#SCHEMA}\": \"$schema\", \"{#DATATYPE}\": \"validated\"},"
     fi

done
echo ${json_lld%,}"]}"
