# Indexation des données raw

Cet outil permet d'indexer les données brutes dans la base seedtree et de rafraichir la vue matérialisée permettant au webservice availability de tenir compte de nouveaux index.

## Déploiement

### Prérequis

  * Accès sans mot de passe à la base `seedtree5` du serveur postgresql `resifinv-prod.u-ga.fr` par les users `seedtree5` et `wsavailability_refresh`
  * L'outil seedtree5 doit être disponible dans le PATH
  * Le point de montage BUD doit être en place (pas nécessaire d'avoir de l'automount)
